package com.hackathlon.rseara.hackathlonproj;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class GameActivity extends ActionBarActivity {


    private int currentPlayer;
    private Map<CharSequence, OurButton> buttonMap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        currentPlayer = 0;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        buttonMap = new HashMap<CharSequence, OurButton>();
        buttonMap.put("but1", new OurButton((ImageButton) findViewById(R.id.but1), 1, 1));
        buttonMap.put("but2", new OurButton((ImageButton) findViewById(R.id.but2), 2, 1));
        buttonMap.put("but3", new OurButton((ImageButton) findViewById(R.id.but3), 3, 1));
        buttonMap.put("but4", new OurButton((ImageButton) findViewById(R.id.but4), 1, 2));
        buttonMap.put("but5", new OurButton((ImageButton) findViewById(R.id.but5), 2, 2));
        buttonMap.put("but6", new OurButton((ImageButton) findViewById(R.id.but6), 3, 2));
        buttonMap.put("but7", new OurButton((ImageButton) findViewById(R.id.but7), 1, 3));
        buttonMap.put("but8", new OurButton((ImageButton) findViewById(R.id.but8), 2, 3));
        buttonMap.put("but9", new OurButton((ImageButton) findViewById(R.id.but9), 3, 3));
        buttonMap.put("but10", new OurButton((ImageButton) findViewById(R.id.but10), 1, 4));
        buttonMap.put("but11", new OurButton((ImageButton) findViewById(R.id.but11), 2, 4));
        buttonMap.put("but12", new OurButton((ImageButton) findViewById(R.id.but12), 3, 4));


    }

    public void buttonOnClickIM(View v) {
        OurButton clickedBut = buttonMap.get(v.getContentDescription());
        int state = clickedBut.getState();
        switch (clickedBut.clickBut()) {
            //case (-1): //chamada sistema
            //  break;
            case (1):
                clickedBut.getImgBut().setImageResource(R.drawable.pecas2);
                break;
            case (2):
                clickedBut.getImgBut().setImageResource(R.drawable.pecas3);
                break;
            case (3):
                clickedBut.getImgBut().setImageResource(R.drawable.pecas4);
                break;
        }

        if (checkWinner()) {
            //end game activity
            Intent intent = new Intent(v.getContext(), WinnerActivity.class);
            intent.putExtra("winner", currentPlayer);
            startActivityForResult(intent, 0);

        } else if(state != 3){
        //next turn activity

            currentPlayer = (currentPlayer+1) % 2;
            System.out.println(currentPlayer);

            ImageView p1 = (ImageView) findViewById(R.id.player1);
                ImageView p2 = (ImageView) findViewById(R.id.player2);


            if(currentPlayer == 0){


        p1.setImageResource(R.drawable.player1white);
        p2.setImageResource(R.drawable.player2black);
         }
            else{
                 p1.setImageResource(R.drawable.player1black);
                p2.setImageResource(R.drawable.player2white);
                }

    }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_game, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private OurButton searchButByXY(int x, int y){
        Iterator<OurButton> iter = buttonMap.values().iterator();
        while(iter.hasNext()){
            OurButton but = iter.next();
            if(but.getX() == x && but.getY() == y)
                return but;
        }
        return null;
    }

    private boolean checkWinner(){
        for(int i = 1; i <= 4; i++)
            if(checkRow(1,i))
                return true;
        for(int i = 1; i <= 3; i++)
            for(int j = 1; j <= 2; j++)
                if(checkCol(i,j))
                    return true;
        if(checkLeftDiag(3,1) || checkLeftDiag(3,2) || checkRightDiag(1,1) || checkRightDiag(1,2))
            return true;
        return false;
    }

    private boolean checkRow(int x, int y) {
        return searchButByXY(x,y).getState() == searchButByXY(x+1,y).getState()
                && searchButByXY(x,y).getState() == searchButByXY(x+2,y).getState()
                && searchButByXY(x,y).getState()!=0;
    }

     private boolean checkCol(int x, int y) {
         return searchButByXY(x,y).getState() == searchButByXY(x,y+1).getState()
                 && searchButByXY(x,y).getState() == searchButByXY(x,y+2).getState()
                 && searchButByXY(x,y).getState()!=0;
}
     private boolean checkLeftDiag(int x, int y) {
         return searchButByXY(x,y).getState() == searchButByXY(x-1,y+1).getState()
                 && searchButByXY(x,y).getState() == searchButByXY(x-2,y+2).getState()
                 && searchButByXY(x,y).getState()!=0;
     }

    private boolean checkRightDiag(int x, int y) {
         return searchButByXY(x,y).getState() == searchButByXY(x+1,y+1).getState()
                && searchButByXY(x,y).getState() == searchButByXY(x+2,y+2).getState()
                && searchButByXY(x,y).getState()!=0;
    }
}
