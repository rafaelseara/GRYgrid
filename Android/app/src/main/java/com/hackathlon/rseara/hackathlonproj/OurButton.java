package com.hackathlon.rseara.hackathlonproj;

import android.widget.ImageButton;

/**
 * Created by rseara on 07/03/15.
 */
public class OurButton {


    private int x;
    private int y;
    private ImageButton imgBut;
    private int state;
    public OurButton(ImageButton b, int x, int y){
        state = 0;
        this.imgBut = b;
        this.x = x;
        this.y = y;
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public ImageButton getImgBut() {
        return imgBut;
    }

    public int getState() {
        return state;
    }

    public int clickBut(){
        if(state < 3){
            state++;
            return state;
        }else{
            return -1;
        }
    }


}
