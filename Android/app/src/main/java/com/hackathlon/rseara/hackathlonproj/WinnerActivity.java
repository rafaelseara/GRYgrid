package com.hackathlon.rseara.hackathlonproj;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;


public class WinnerActivity extends ActionBarActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winner);

        int winner = 0;

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            winner = extras.getInt("winner");
        }

        if (winner == 1)
            ((ImageView) findViewById(R.id.imageView)).setImageResource(R.drawable.p2winner);

    }
    public void onClickPlayAgain(View v){
        Intent intent = new Intent(v.getContext(), GameActivity.class);
        startActivityForResult(intent, 0);
    }
    public void onClickMenu(View v){
        Intent intent = new Intent(v.getContext(), MenuActivity.class);
        startActivityForResult(intent, 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_winner, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
